//Ungureanu Iulian

import java.util.ArrayList;

public class Judet {
	String nume;
	Tara country;  					//Atribut Tara, fiecare judet poate apartine unei singure tari.
	ArrayList<Oras> lista_orase; 	//Lista cu orasele care apartin unui Judet, un Judet poate avea mai multe orase.
	
	//Constructori (cu si fara parametrii)
	public Judet(){
		lista_orase = new ArrayList<Oras>(); 
	}
	
	public Judet(String nume, Tara country) {
		lista_orase = new ArrayList<Oras>();
		this.nume = nume;
		this.country = country;
		this.country.addJudet(this);  //Se apeleaza functia din clasa Tara, si adauga obiectul Judet in lista.
	}
	
	//Get si set.
	public String getNume() {
		return nume;
	}

	public void setNume(String nume) {
		this.nume = nume;
	}

	public Tara getCountry() {
		return country;
	}

	public void setCountry(Tara country) {
		this.country = country;
	}

	public ArrayList<Oras> getLista_orase() {
		return lista_orase;
	}

	//Functia se apeleaza in constructorul orasului, si va adauga orasul in lista pentru judetul respectiv.
	public void addOras(Oras city) {
		this.lista_orase.add(city);
	}
}
