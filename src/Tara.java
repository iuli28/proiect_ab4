//Ungureanu Iulian

import java.util.ArrayList;

public class Tara {
	private String nume;
	private ArrayList<Judet> lista_judete;  //Lista pentru tinerea evidentei judetelor dintr-o tara.
	
	//Constructori
	public Tara(){
		this.lista_judete = new ArrayList<Judet>();
	}

	public Tara(String nume) {
		this.nume = nume;
		this.lista_judete = new ArrayList<Judet>();
	}

	//Get si Set.
	public String getNume() {
		return nume;
	}

	public void setNume(String nume) {
		this.nume = nume;
	}

	public ArrayList<Judet> getLista_judete() {
		return lista_judete;
	}
	
	//Functia va fi apelata in constructorul Judetului si va adauga obiectul in lista.
	public void addJudet(Judet j){
		lista_judete.add(j);
	}
}
	

	
