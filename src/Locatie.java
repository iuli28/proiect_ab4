//Ungureanu Iulian

import java.util.ArrayList;

public class Locatie implements Comparable<Locatie>{
	//Declararea atributelor din cerinta.
	private String nume;						
	private Oras city;
	private float pret;
	private ArrayList<String> activitati;
	
	//Perioada am definit-o prin doua atribute: data_inceput si data_sfarsit
	private String data_inceput;
	private String data_sfarsit;
	
	//Am adaugat numarul de zile pentru rezolvarea cerintei 3, dar cred ca este si o informatie relevanta.
	private int nr_zile;
	
	//Constructori
	public Locatie(){
		this.activitati = new ArrayList<String>();
	}
	
	public Locatie(String nume, Oras city, float pret, String data_inceput, String data_sfarsit, int nr_zile , ArrayList<String> activitati) {
		this.nume = nume;
		this.city = city;
		this.pret = pret;
		this.data_inceput = data_inceput;
		this.data_sfarsit = data_sfarsit;
		this.nr_zile = nr_zile;
		this.activitati = new ArrayList<String>();
		this.activitati = activitati;
		city.addLocatie(this);						//Apelare functie care adauga locatia, orasului din care face parte.
	}
	
	//Get si set.
	public String getNume() {
		return nume;
	}

	public void setNume(String nume) {
		this.nume = nume;
	}

	public Oras getCity() {
		return city;
	}

	public void setCity(Oras city) {
		this.city = city;
	}

	public float getPret() {
		return pret;
	}

	public void setPret(float pret) {
		this.pret = pret;
	}
	
	public String getData_inceput() {
		return data_inceput;
	}

	public void setData_inceput(String data_inceput) {
		this.data_inceput = data_inceput;
	}

	public String getData_sfarsit() {
		return data_sfarsit;
	}

	public void setData_sfarsit(String data_sfarsit) {
		this.data_sfarsit = data_sfarsit;
	}

	public int getNr_zile() {
		return nr_zile;
	}

	public void setNr_zile(int nr_zile) {
		this.nr_zile = nr_zile;
	}
	
	public ArrayList<String> getActivitati() {
		return activitati;
	}

	public void setActivitati(ArrayList<String> activitati) {
		this.activitati = activitati;
	}
	
	
	//Functia de afisare pentru informatii despre locatia cu numele X.
	public void afisare(){
		System.out.println("Nume: " + this.nume);
		
		//Obtinerea numelui Tarii si Judetului a fost apelara ierarhic. Din oras se obtine judetul si numele sau, apoi din Judet se obtine Tara.
		//Apelul: this.city.getJ().getCountry().getNume()
		
		System.out.println("Oras: " + this.city.getNume() + ", Judet: " + this.city.getJ().getNume() + ", Tara: "+ this.city.getJ().getCountry().getNume());
		System.out.println("Pret meduiu zi: " + this.pret + ", Perioada: " + this.data_inceput + " - " + this.data_sfarsit);
		System.out.print("Activitati: ");
		for(String a : activitati){
			System.out.print(a + ", ");
		}
		System.out.println();
	}
	
	//Fucntie da afisare penru top 5 locatii.La fel ca mai sus, doar ca se modifica pretul.
	//Se cerea pretul total asa ca am scris la pret total: pretul mediu * nr_zile.
	public void afisare_top(){
		System.out.println("Nume: " + this.nume);
		System.out.println("Oras: " + this.city.getNume() + ", Judet: " + this.city.getJ().getNume() + ", Tara: "+ this.city.getJ().getCountry().getNume());
		System.out.println("Pret total vacanta: " + this.pret * this.nr_zile + ", Perioada: " + this.data_inceput + " - " + this.data_sfarsit + ", Numar zile: " + this.nr_zile);
		System.out.print("Activitati: ");
		for(String a : activitati){
			System.out.print(a + ", ");
		}
		System.out.println();
	}

	//Definirea functiei compareTo, din interfata Comparable pentru a ordona lista de locatii dupa cel mai mic pret mediu.
	@Override
	public int compareTo(Locatie arg0) {
		if(this.getPret() < arg0.getPret())
			return -1;
		else
			return 1;
	}

}
