//Ungureanu Iulian

import java.util.ArrayList;
import java.util.Collections;
import java.util.Hashtable;
import java.util.List;
import java.util.Objects;
import java.util.Set;


public class App {
	
	//Functia de afisare a celor mai bune 5 locatii. Se dau ca parametrii locatile sortate(pret crescator) si datele.
	static void afisare_locatii(List<Locatie> locatii, String data_a, String data_b){
		int k = 0; 
		
		//Se parcurge fiecare locatie.
		for(Locatie item : locatii){
			//Se verifica daca datele corespund.
			if((Objects.equals(item.getData_inceput(), data_a)) && (Objects.equals(item.getData_sfarsit(), data_b))){
				
				//Daca corespund se afiseaza informatile despre locatie si se incremnteaza k (contor locatii).
				item.afisare_top();
				System.out.println();
				k++;
			}
			
			//Daca s-au gasit 5 locatii se iese din for printr-un break.
			if (k == 5)
				break;
		}
		
		//Daca nu s-a gasit nicio locatie se afiseaza un mesaj.
		if(k == 0)
			System.out.println("Nu exista rezultate!");
	}
	
	//Functia pentru cerinta 1, afisare informatii locatia X.
	static void informatii(Hashtable<String, Locatie> locatie, String x){
		
		//Se verifica daca exista locatia respectiva in lista.
		if (locatie.get(x) != null)
			locatie.get(x).afisare();
		else
			System.out.println("Locatia nu exista!");
	}
	
	
	//Functia pentru cerinta 2 , afisare cele mai bune 5 locatii sortate dupa pret si cautate dupa oras.
	static void sortare_oras(Hashtable<String, Oras> lista_oras, String x, String data_a, String data_b){
		
		//Se verifica daca exista orasul!
		if(lista_oras.get(x) == null){
			System.out.println("Orasul nu a fost gasit!");
			return;
		}
		
		//Se creeaza lista cu locatii, care se obtine prin apelarea: getLista_locatii();
		List<Locatie> locatii = new ArrayList<Locatie>();
		locatii = lista_oras.get(x).getLista_locatii();
		
		//Se ordoneaza lista conform cerintei.
		Collections.sort(locatii);
		
		//Se afiseaza locatile.
		afisare_locatii(locatii, data_a, data_b);	
	}
	
	//Functia pentru cerinta 2 , afisare cele mai bune 5 locatii sortate dupa pret si cautate dupa judet.
	static void sortare_judet(Hashtable<String,Judet> lista_judet, String x, String data_a, String data_b){
		
		//Se verifica daca exista judetul.
		if(lista_judet.get(x) == null){
			System.out.println("Judetul nu a fost gasit!");
			return;
		}
		
		//Se obtine intai lista de orase.
		ArrayList<Oras> lista_oras = new ArrayList<Oras>();
		lista_oras = lista_judet.get(x).getLista_orase();
	
		List<Locatie> locatii = new ArrayList<Locatie>();
		//Se adauga locatile fieacrui oras in lista de locatii. Deoarce fiecare oras are o lista vom folosi :addAll
		for(Oras item : lista_oras){
			locatii.addAll(item.getLista_locatii());
		}
		
		//Se sorteaza si se afiseaza.
		Collections.sort(locatii);
		afisare_locatii(locatii, data_a, data_b);	
	}
	
	//Functia pentru cerinta 2 , afisare cele mai bune 5 locatii sortate dupa pret si cautate dupa tara.
	static void sortare_tara(Hashtable<String, Tara> lista_tara, String x, String data_a, String data_b){
		
		//Verificare existena tara.
		if(lista_tara.get(x) == null){
			System.out.println("Tara nu a fost gasita!");
			return;
		}
		
		//Pe principiul de mai sus, intai se obtine lista cu judete, apoi se obtine lista cu toate orasele
		//Apoi din fiecare oras se obtine lista cu locatile specifice si se concateneaza.
		
		ArrayList<Judet> list_judet = new ArrayList<Judet>();
		list_judet = lista_tara.get(x).getLista_judete();
		
		ArrayList<Oras> list_oras = new ArrayList<Oras>();
		for(Judet item : list_judet){
			list_oras.addAll(item.getLista_orase());
		}
		
		List<Locatie> locatii = new ArrayList<Locatie>();
		for(Oras item : list_oras){
			locatii.addAll(item.getLista_locatii());
		}
		
		//Sortare si afisare
		Collections.sort(locatii);
		afisare_locatii(locatii, data_a, data_b);
		
	}
	
	//cerinta 3 , cea mai ieftina activitate pentru 10 zile.
	static void activitate_ieftin(Hashtable<String, Locatie> locatie, String x){
		List<Locatie> locatii = new ArrayList<Locatie>();
		
		//Functia primeste ca parametur un Hash, astfel se obtin toate key-urile din hash.
		//Dupa pentru fiecare key se obtine valoarea. Care este o Locatie si se adauga in lista.
		Set<String> keys= locatie.keySet();
		for(String key : keys){
			locatii.add(locatie.get(key));
		}
		
		//Intai sortam lista dupa pret descrescator.
		Collections.sort(locatii);
		
		for(Locatie item : locatii){
			
			//In acest moment la prima locatie care contine activitatea cautata si are numarul de zile 10 , se afiseaza si se paraseste functia.
			//Deoarece lista era ordonata dupa cel mai mic pret, cu siguranta aceasta e cea mai ieftina.
			if((item.getActivitati().contains(x)) && (item.getNr_zile() == 10)){
				item.afisare_top();
				return;
			}
		}
		
		//Daca nu se gaseste nicio locatie se afiseaza un mesaj.
		System.out.println("Nu s-a gasit locatie!");
	}
	

	public static void main(String[] args) {
		/*Am declarat datele in main pentru a-mi usura un pic munca, recunosc :).
		De asemenea am stocat Orasele, Judetele si Tarile intr-un HashTable, deoarece cautarea se face dupa nume.
		Astfel am pus key, un string cu numele, si la valoare obiectl in sine. Este mai eficient cu o functie de hash, decat daca as fi
		parcurs toata lista pana la gasirea obiectului. */
		
		//Am declarat cateva liste de activitati.
		ArrayList<String> a1 = new ArrayList<String>(); a1.add("Ski"); a1.add("trasee_montane");
		ArrayList<String> a2 = new ArrayList<String>(); a2.add("plaja"); a2.add("surf");
		ArrayList<String> a3 = new ArrayList<String>(); a3.add("inot"); a3.add("plaja"); a3.add("spa");
		ArrayList<String> a4 = new ArrayList<String>(); a4.add("city-break");
		
		//Adugare tari.
		Tara country = new Tara("Romania"); Tara country1 = new Tara("Franta");
		Hashtable<String, Tara> lista_tara = new Hashtable<String, Tara>();
		lista_tara.put(country.getNume(), country); lista_tara.put(country1.getNume(), country1);
		
		//Adaugare judete.
		Judet j1 = new Judet("Prahova", country); Judet j2 = new Judet("Constanta", country); Judet j3 = new Judet("Cluj", country);
		Hashtable<String, Judet> lista_judet = new Hashtable<String, Judet>();
		lista_judet.put(j1.getNume(), j1); lista_judet.put(j2.getNume(), j2); lista_judet.put(j3.getNume(), j3);
		
		//Adaugare orase.
		Oras city1 = new Oras("Sinaia", j1); Oras city2 = new Oras("Predeal", j1); Oras city3 = new Oras("Constanta", j2);
		Oras city4 = new Oras("Mamaia", j2); Oras city5 = new Oras("Venus", j2);  Oras city6 = new Oras("Cluj-Napoca",j3);
		Hashtable<String, Oras> lista_oras = new Hashtable<String, Oras>(); 
		lista_oras.put(city1.getNume(), city1); lista_oras.put(city2.getNume(), city2); lista_oras.put(city3.getNume(), city3);
		lista_oras.put(city4.getNume(), city4); lista_oras.put(city5.getNume(), city5); lista_oras.put(city6.getNume(), city6);
	
		//Adaugare locatii.
		Locatie l1 = new Locatie("Aventura1", city1, 254, "2 ianuarie", "5 ianuarie", 4, a1); 
		Locatie l2 = new Locatie("Aventura2", city1, 320, "2 ianuarie", "5 ianuarie", 4, a1);
		Locatie l3 = new Locatie("Aventura3", city1, 147, "2 ianuarie", "5 ianuarie", 4, a1);
		Locatie l4 = new Locatie("Aventura4", city1, 236, "2 ianuarie", "11 ianuarie", 10, a1);
		Locatie l5 = new Locatie("Aventura5", city1, 289, "2 ianuarie", "5 ianuarie", 4, a1);
		Locatie l6 = new Locatie("Aventura6", city1, 215, "2 ianuarie", "5 ianuarie", 4, a1);
		Locatie l7 = new Locatie("Aventura7", city2, 209, "2 ianuarie", "11 ianuarie", 10, a1);
		Locatie l8 = new Locatie("Aventura8", city2, 178, "2 ianuarie", "5 ianuarie", 4, a1);
		Locatie l9 = new Locatie("Aventura9", city3, 209, "2 ianuarie", "11 ianuarie", 10, a2);
		Locatie l10 = new Locatie("Aventura10", city3, 178, "2 ianuarie", "5 ianuarie", 4, a2);
		Locatie l11 = new Locatie("Aventura11", city4, 352, "2 ianuarie", "11 ianuarie", 10, a2);
		Locatie l12 = new Locatie("Aventura12", city4, 212, "2 ianuarie", "11 ianuarie", 10, a3);
		Locatie l13 = new Locatie("Aventura13", city4, 198, "2 ianuarie", "5 ianuarie", 4, a2);
		Locatie l14 = new Locatie("Aventura14", city5, 312, "2 ianuarie", "11 ianuarie", 10, a2);
		Locatie l15 = new Locatie("Aventura15", city5, 214, "2 ianuarie", "11 ianuarie", 10, a3);
		Locatie l16 = new Locatie("Aventura16", city5, 148, "2 ianuarie", "5 ianuarie", 4, a2);
		Locatie l17 = new Locatie("Aventura17", city6, 250, "2 ianuarie", "11 ianuarie", 10, a4);
		Locatie l18 = new Locatie("Aventura18", city6, 168, "2 ianuarie", "5 ianuarie", 4, a4);
		Hashtable<String, Locatie> lista_locatie = new Hashtable<String, Locatie>(); 
		lista_locatie.put(l1.getNume(), l1); lista_locatie.put(l2.getNume(), l2); lista_locatie.put(l3.getNume(), l3); 
		lista_locatie.put(l4.getNume(), l4); lista_locatie.put(l5.getNume(), l5); lista_locatie.put(l6.getNume(), l6);
		lista_locatie.put(l7.getNume(), l7); lista_locatie.put(l8.getNume(), l8); lista_locatie.put(l9.getNume(), l9);
		lista_locatie.put(l10.getNume(), l10); lista_locatie.put(l11.getNume(), l11); lista_locatie.put(l12.getNume(), l12);
		lista_locatie.put(l13.getNume(), l13); lista_locatie.put(l14.getNume(), l14); lista_locatie.put(l15.getNume(), l15);
		lista_locatie.put(l16.getNume(), l16); lista_locatie.put(l17.getNume(), l17); lista_locatie.put(l18.getNume(), l18);
		
		//Nu e chiar un coding style corect (nu prea este lizibil) la adugarea dateleor, dar le am scris mai compact ca sa ocupe spatiu mai putin.
		 
		//Am scris cateva teste pentru evidentierea cerintelor.
		
		// #test1
		System.out.println("#test1 - Testare cerinte");
		System.out.println("\n" + "Infromatii locatie:");
		informatii(lista_locatie, "Aventura10");
		System.out.println("\n" + "Cautare dupa oras:");
		sortare_oras(lista_oras, "Sinaia", "2 ianuarie", "5 ianuarie");
		System.out.println("\n" + "Cautare dupa judet:");
		sortare_judet(lista_judet, "Prahova", "2 ianuarie", "5 ianuarie");
		System.out.println("\n" + "Cautare dupa tara:");
		sortare_tara(lista_tara, "Romania", "2 ianuarie", "5 ianuarie");
		System.out.println("\n" + "Cea mai ieftina activitate");
		activitate_ieftin(lista_locatie, "spa");
		
		// #test2
		System.out.println("\n-----------------------------------------");
		System.out.println("#test2 - Nume de locatii introduse gresit");
		System.out.println("\n" + "Infromatii locatie:");
		informatii(lista_locatie, "Aventura16");
		System.out.println("\n" + "Cautare dupa oras:");
		sortare_oras(lista_oras, "gresit", "2 ianuarie", "5 ianuarie");
		System.out.println("\n" + "Cautare dupa judet:");
		sortare_judet(lista_judet, "gresit", "2 ianuarie", "5 ianuarie");
		System.out.println("\n" + "Cautare dupa tara:");
		sortare_tara(lista_tara, "gresit", "2 ianuarie", "5 ianuarie");
		System.out.println("\n" + "Cea mai ieftina activitate:");
		activitate_ieftin(lista_locatie, "city-break");
		
		// #test3
		System.out.println("\n-----------------------------------------");
		System.out.println("#test3 - Activitate introdusa gresit/ schimbare date");
		System.out.println("\n" + "Infromatii locatie:");
		informatii(lista_locatie, "Aventura18");
		System.out.println("\n" + "Cautare dupa oras:");
		sortare_oras(lista_oras, "Mamaia", "2 ianuarie", "5 ianuarie");
		System.out.println("\n" + "Cautare dupa judet:");
		sortare_judet(lista_judet, "Constanta", "2 ianuarie", "11 ianuarie");
		System.out.println("\n" + "Cautare dupa tara:");
		sortare_tara(lista_tara, "Romania", "2 ianuarie", "11 ianuarie");
		System.out.println("\n" + "Cea mai ieftina activitate:");
		activitate_ieftin(lista_locatie, "gresit");
		
		// #test4
		System.out.println("\n-----------------------------------------");
		System.out.println("#test4 - Perioada/activitate introdusa gresit");
		System.out.println("\n" + "Infromatii locatie:");
		informatii(lista_locatie, "gresit");
		System.out.println("\n" + "Cautare dupa oras:");
		sortare_oras(lista_oras, "Mamaia", "2 ianuarie", "7 ianuarie");
		System.out.println("\n" + "Cautare dupa judet:");
		sortare_judet(lista_judet, "Constanta", "6 ianuarie", "11 ianuarie");
		System.out.println("\n" + "Cautare dupa tara:");
		sortare_tara(lista_tara, "Romania", "2 ianuarie", "11 ianuarie");
		System.out.println("\n" + "Cea mai ieftina activitate:");
		activitate_ieftin(lista_locatie, "inot");
		
		
	}

}
