//Ungureanu Iulian

import java.util.ArrayList;

public class Oras {
	private String nume;
	private Judet j; 							//Un oras apartine unui singur Judet.
	
	private ArrayList<Locatie> lista_locatii;	//Un oras poate contine mai multe locatii, si creem o lista cu acestea.
	
	//Constructori (cu si fara parametrii)
	public Oras(){
		lista_locatii = new ArrayList<Locatie>();
	}

	public Oras(String nume, Judet j) {
		lista_locatii = new ArrayList<Locatie>();
		this.nume = nume;
		this.j = j;
		this.j.addOras(this);					//Apelare functie din clasa Judet care adauga obiectul "Oras" in lista Judetului din care face parte.
	}

	//Get si set.
	public String getNume() {
		return nume;
	}

	public void setNume(String nume) {
		this.nume = nume;
	}

	public Judet getJ() {
		return j;
	}

	public void setJ(Judet j) {
		this.j = j;
	}
	
	public ArrayList<Locatie> getLista_locatii() {
		return lista_locatii;
	}

	//Functia care va adauga o locatie a unui oras in lista.
	public void addLocatie(Locatie l){
		lista_locatii.add(l);
	}
}
